---
layout: defaultCustom
title: How
permalink: /how/
type: how
---

<section class="posts">
    <ul>
    {% for pg in site.hows %}
      <h2>
        <a class= {{ pg.type }} href="{{ pg.url }}">
          {{ pg.title }}
        </a>
      </h2>
      <p>{{ pg.summary }}</p>
    {% endfor %}
 
    </ul>
</section>
    
    