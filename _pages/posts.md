---
layout: default
permalink: /posts/
---

<section class="posts">
<ul>
{% for post in site.posts %}
<li
style="
{% if post.type == "text" %}
color:#151515;
{% elsif post.type == "image" %}
color: #FE2E64;
{% endif %}
"
>
<a href="{{ site.baseurl }}{{ post.url }}" 
style="
{% if post.type == "text" %}
color:#151515;
{% elsif post.type == "image" %}
color: #FE2E64;
{% endif %}
"
>
{% if post.title == "untitled" %}
{{ post.url | slice: 11, 20}}
{% else %}
{{ post.title }}
{% endif %}
</a>
<time datetime="{{ post.date | date_to_xmlschema }}">{{ post.date | date: "%Y-%m-%d" }}</time></li>
{% endfor %}
</ul>
</section>
