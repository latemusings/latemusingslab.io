---
layout: page
title: About
---

Estamos hartos de las redes sociales y sus dinámicas fagocitadoras de atención y clicks. También queríamos aprender a usar Jekyll.

*Si quieres contactarnos, escríbenos [acá.](https://pad.riseup.net/p/latecomments-keep)*

(El tema que usamos en esta página se llama Sidey. [Acá está.](https://github.com/ronv/sidey))