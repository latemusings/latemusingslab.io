---
title: Zettelkasten
layout: post
type: text
description: 
summary: Caminante, son tus huellas el camino y nada más.
tags: [zettelkasten]
date: 2021-04-08
image: 
---

Hace ya varios meses descubrimos la idea del [zettelkasten](https://es.wikipedia.org/wiki/Zettelkasten) y, como es habitual con estos temas de organización, jerarquización y estucturación de información, caímos en el hoyo del conejo, en la vieja trampa de perder el tiempo buscando la forma definitiva de no perderlo. 

La idea era tentadora: un sistema para tomar notas que, a base de un uso juicioso y estructurado, termine convirtiéndose en un segundo cerebro al que podemos acudir en busca de ideas originales, conexiones y epifanías. Nunca más una hoja en blanco. Nunca más la vaga sensación de que en algún momento en el pasado leímos o pensamos eso que terminaría por darle solidez al texto o la idea que estamos tratando de cristalizar, pero lamentablemente olvidamos exactamente qué era. Un zettelkasten hace de esto algo lograble.

El problema fue perdernos en busca de la implementación _más_ fiel, _más_ eficiente, _más_ efectiva, como si hubiera una forma _correcta_ de hacer las cosas. Un sistema tan ambicioso como este, tan potencialmente poderoso (¡un "segundo cerebro"!) exigía, creíamos, una ejecución rigurosa. Obviamente, esto era una quimera, pero era inevitable ceder a la tentación de buscarla por un rato (un rato que terminó creciendo hasta convertirse en un periodo de varios meses). Como era de esperar, el hoyo del conejo terminó por envolvernos de tal forma que el tiempo que queríamos usar implementando cosas como esta terminó siendo empleado en la investigación de cada minucia que pasaba frente a nosotros.

No diremos que fue en vano. Sin embargo, sí podríamos habernos ahorrado mucho tiempo si se nos hubiera advertido sobre la inutilidad de buscar respuestas definitivas. Y se nos advirtió: internet está lleno de textos como este que intentan explicarnos por qué es mejor hacer camino al andar, pero, por supuesto, hicimos oídos sordos.

Quede esto entonces como mensaje de advertencia cuyo destino es ser pasado de largo despreocupadamente.

**¿Quieres explorar el hoyo del conejo?**

Echa un vistazo al hoyo primero: no es necesario hundirse en él:

[Busca](https://www.youtube.com/watch?v=wFZHuWLA09M) [un](https://medium.com/voces-en-espa%C3%B1ol/zettelkasten-c%C3%B3mo-un-erudito-alem%C3%A1n-fue-tan-incre%C3%ADblemente-productivo-b16643e170cc) [resumen](https://emowe.com/cerebro-digital/como-tomar-notas-apuntes-zettelkasten/) [corto](https://writingcooperative.com/zettelkasten-how-one-german-scholar-was-so-freakishly-productive-997e4e0ca125) sobre cómo funciona un Zettelkasten. Si quieres ahondar, lee [el libro de Ahrens](https://www.goodreads.com/book/show/34507927-how-to-take-smart-notes) (libgen es tu amigo).

Ahora, si quieres caer en el hoyo:

[Esta página está llena de recursos y discusiones sobre el tema](https://zettelkasten.de/posts/overview/)

[El subreddit sobre el tema](https://www.reddit.com/r/Zettelkasten/) (y hay uno para cada aplicación)

[Jardines digitales, una idea muy parecida (y fascinante)](https://nesslabs.com/digital-garden-set-up)