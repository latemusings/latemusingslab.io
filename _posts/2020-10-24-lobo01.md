---
layout: post
title: Tratado de las pasiones del alma
description: Impresiones iniciales.
summary: Decidimos volver a leer a Lobo Antunes. Un poco confundidos, tratamos de ponerle orden a nuestras ideas.
tags: [retazos, libros, antonio lobo antunes, tratado de las pasiones del alma]
type: text
---


_"fuera las fachadas se separaban a duras penas de las neblinas de la aurora y los arboles, consumidos en ese mes del año, se eleaban de las tinieblas con sus muñones al hombro."_
<div style="text-align: right">
(Tratado de las pasiones del alma, 13)
</div>

<br/>

La prosa de Lobo Antunes tiene, en medio de sus ires y venires, ciertos rayos de plasticidad que iluminan lo que es, ante todo, un denso y enrevesado torrente verbal.

Leo la oración anterior y me estremezco al ver cómo caigo en florituas innecesarias. Puede ser la influencia de una escritura que se siente barroca a base del deshábito causado por meses (¿años?) de lecturas de hot takes irrelevantes y textos preocupados por el corto espacio de atención que podemos dedicarles. Caemos en cuenta entonces que este será un ejercicio tanto de escritura como de lectura. No nos hacemos muchas esperanzas, pero tampoco vamos a desaprovechar el impulso: quizá la inercia nos haga tropezar con alguna cosa que podamos rescatar algún día.

Leer a Lobo Antunes te exige. O por lo menos se siente así después de habernos alejado de la lectura de literatura por ya un buen rato. Decidimos empezar a leer _Tratado de las pasiones del alma_ (vaya título. Ya veremos si el texto -y el lector- le hace justicia) y comprobar la muerte definitiva de nuestro attention span o celebrar su resurgimiento.

El prefacio resultó un golpe que, aunque esperado (ya habíamos leído a Lobo Antunes en el pasado: sabíamos a qué nos íbamos a enfrentar), nos desestabilizó al punto de cuestionarnos la tarea desde las primeras páginas. Confusos y perdidos en medio de diálogos intercalados con flujos de conciencia, la prosa de este autor nos hace sentir inadecuados (_¿qué está sucediendo?, ¿qué significa esta palabra?, ¿y esta otra?, ¿y esta otra también?, ¿será que no entendemos porque tenemos un léxico muy pobre?, ¿quién está hablando?, ¿es esto un diálogo?, ohpordiosfracasécomolector..._), pero por momentos surgen rayos de luz que nos incentivan a seguir en un ejercicio que se siente, muy literalmente, como circular por el espacio entre la vigilia y el sueño, donde lo leído se mezcla con nuestro propio flujo de conciencia y nos cuesta trabajo distinguir una cosa de la otra.

Después pasamos al primer capítulo y, aunque el estilo sigue siendo el mismo, nos preguntamos si el prefacio era una prueba para comprobar si realmente estábamos dispuestos a continuar con la lectura. Por fortuna continuamos y empezamos a identificar, tentativamente, las claves de lectura: la ausencia de nombres (exceptuando topónimos, que para nosostros es como si no existieran); la identificación de los personajes con sustantivos comunes, como si estuviéramos hablando de arquetipos en la cabeza de alguien más que de personas; el vaivén entre el presente narrado y la memoria de los personajes y cómo estos dos mundos terminan siendo el mismo tanto para los personajes como para nosotros (un poco como cuando durante un sueño en alguien confluyen dos o más identidades con la mayor naturalidad); los espacios del pasado (la casa de la infancia, un sótano donde se conspira con otros) como escenarios que inundan, se superponen incluso a los espacios del presente en donde los personajes actúan con una cierta inercia, perdidos en sus remembranzas interiores…

El pasado. Ante todo el pasado. O más que el pasado, los recuerdos.

Bueno, por lo menos en las 30 páginas que llevamos leídas. 