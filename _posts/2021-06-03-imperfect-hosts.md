---
layout: post
title: The Sandman 02 - Imperfect hosts
description: 
summary: One question, one answer. The rules, my lord.
tags: [the sandman, libros, comics, retazos]
date: 2021-06-02
image: "/assets/images/2021/02imperfecthosts.jpg"
type: text
---

_"I release something that I created before the dawn of time. Re-absorb that fragment of myself I placed inside it."_

¿Qué guardamos en los sueños? ¿Qué parte de nosotros termina por quedarse en ellos? ¿Cuál parte resiste este impulso? ¿Podemos recobrar aquello que dejamos en ellos?

Es curioso que la narración de sueños se sienta tan urgente y significativa para quien los vivió y tan inocua y anodina para quien la escucha. Siempre he creído que es un fenómeno que da cuenta de esa distancia abismal que hay entre unos y otros, de esa distancia entre conciencias apenas unidas frágilmente por un lenguaje a todas luces insuficiente. Mientras tu sueño en potencia revela las partes más urgentes de ti mismo, al mismo tiempo oculta tras múltiples velos cualquier significado para un observador (entre los que, por supuesto, estás incluido) incapaz de hacer contacto con ese mundo que los sueños están revelando. 

Cuando alguien nos cuenta un sueño nos está mostrando una parte tan profunda de sí mismo que es probable que en ella no penetre luz alguna, que no veamos nada, que ni siquiera el soñador mismo sea capaz de verla. Pero allí hay algo, una parte fundamental, urgente, que clama ser vista y sin embargo olvidamos e ignoramos con facilidad. 

Y así como guardamos cosas en los sueños, cosas que en amaneceres recobramos vaga y fugazmente, los sueños también guardan en nosotros cosas que descubrimos inadvertidamente en comentarios aparentemente anodinos, en reacciones inesperadas, en lapsus que pocas veces identificamos a tiempo. Entonces, una escucha atenta, una atención aguda es necesaria para que podamos notar esas fuerzas que delatan nuestros sueños y así poder penetrar un poco más dentro de nuestro ser o, mejor aún, reabsorber eso que plantamos en ellos sin saberlo siquiera. Reabsorberlo y con ello obtener el poder que nos da el conocimiento de ese fragmento de nosotros mismos que actuaba por sí solo y ahora podemos conectar con nuestra propia escencia.

Reabsorber*lo*.

Reabsorber*nos*.