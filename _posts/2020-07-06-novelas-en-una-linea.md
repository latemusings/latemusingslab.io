---
layout: post
title: Novelas en menos de una línea
description: 
summary: oficinista local se rehusa a hacer su trabajo (y todo lo demás).
tags: [libros, prompts]
date: 2020-07-06 17:30:00 -05:00
image: "/assets/images/novels.png"
type: text
---

- oficinista local se rehusa a hacer su trabajo (y todo lo demás).
- un hombre deprimido se aburre. lee, sueña y escribe mientras tanto.
- pedante con spleen metafísico va a europa y vuelve con el rabo entre las piernas a trabajar en un manicomio.
- un tipo y una chica se caen gordos pero se terminan encuentando.
- un hombre huye hacia la selva y se pierde.
- los humanos tratan de colonizar un planeta y fracasan.
- un señor viaja río abajo y vuelve a subir.
- el señor del río monta un puteadero con una amiga.
- el mismo señor del río y del puteadero se muere.
- un tipo descubre que le gusta la lectura, lo cual supone un grave peligro.
- bohemio desencantado conoce chica trauma. chica trauma desaparece.
- un grupo de gente no escribe.
- oficinista no puede ir al trabajo y se le cae el mundo por ello.
- un tipo mata a otro por no ser capaz de ponerse gafas de sol o una visera.
- hombre mata a mujer porque nadie lo entiende :(
- un incel rumia amarguras.
- un par de hermanos van por la vida frustrados sexualmente. por el camino descubren un camino a la evolución de la raza humana.
- un escritor espera a que su pareja llegue del trabajo toda una noche. cuando llegue, se acaba la novela.
