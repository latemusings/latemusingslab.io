---
layout: post
title: Arranquemos
description: Declaración de intenciones.
summary: Declaración de (someras) intenciones.
tags: [retazos]
type: text
---

Para escribir siempre vamos a encontrar alguna excusa: no nos alcanza el tiempo, estamos muy cansados, el lápiz no se siente bien, el gato pide atención, un cigarro y después sí empezamos, una pandemia de proporciones que aún no logramos entender...


Usamos como excusa últimamente nuestra molestia con las plataformas dominantes, que acaparan la mayor parte de la conversación pública como si fuéramos ratas en cajas de Skinner. Esta página es un intento de salir de la caja. Así fuera de ella busquemos presionar los mismos botones.
