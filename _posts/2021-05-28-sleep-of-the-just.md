---
layout: post
title: The Sandman 01 - Sleep of the just
description: 
summary: Wake up, sir. We're here
tags: [the sandman, libros, comics, retazos]
date: 2021-05-31
image: 
type: text
---

:-------------------------:|:-------------------------:
![](/assets/images/2021/01Wakeup.png)  |  ![](/assets/images/2021/CoinAndSong3.png)


_I give you a knife from under the hills_

_and a stick that I stuck through a dead man's eye._

_I give you a claw I ripped from a rat._

_I give you a name, and the name is Lost._

_I give you blood from out of my vein,_

_and a feather I pulled from an angel's wing._

<br/><br/>

***Despierta.***

No es gratuito que las primeras palabras con las que arranca ***The Sandman*** sean un llamado a despertar: _"Wake up, sir. We're here"_. ¿Despertar de qué? ¿Dónde estamos? Estamos en Inglaterra en 1916. Estamos entrando en un mundo de intrigas, poder y fuerzas ocultas, un mundo de ambiciones y saberes arcanos. Estamos en el mundo de Roderick Burgess, Daemon King, poderoso ocultista con pretensiones de atrapar a la mismísima muerte. Estamos en medio de una historia que acaba de empezar (o eso creemos). 

Pero, volvamos a la primera pregunta, quizá más reveladora, pues un _¿dónde estamos?_ siempre puede ser respondido sin realmente revelarse nada: estamos *acá* (o *allá*, qué mas da). Inevitabemente la pregunta que sigue termina siendo un _¿y ahora qué?_ ¿Despertar de qué, entonces? De un sueño, naturalmente. Y es que es importante desde ya entender los sueños no sólo como ese territorio que recorremos mientras dormimos, sino como esa amalgama de fuerzas, pulsiones, símbolos e historias que Gaston Bachelard definía como *"manifestaciones del ánima"*. Manifestaciones en las que vivimos envueltos sin cesar, por lo que el llamado a despertar no establece antagonismo alguno entre sueño y vigilia, sino un tránsito entre dos mundos análogos. Despierta: sal de un mundo para _entrar_ a otro. 

Despierta, parece decirle el el conductor al Doctor Hathaway cuando realmente está dirigiéndose a nosotros, lectores desprevenidos. Y así como Hathaway despierta, bosteza y entra en el mundo de horror que le depara su visita a Burgess, nosotros estamos entrando en sintonía con ese mismo mundo: estamos temporalmente saliendo de nuestro mundo para entrar, para despertar, en el mundo de ***The Sandman.***