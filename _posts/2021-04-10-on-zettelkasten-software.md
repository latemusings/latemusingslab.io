---
title: On zettelkasten software
layout: post
type: text
description: 
summary: tl;dr - No importa qué escojas, decídete por una herramienta y no mires atrás.
tags: [zettelkasten, software]
date: 2021-04-10
image: 
---

***tl;dr:*** *No importa qué escojas, decídete por una herramienta y no mires atrás.*

Roam Research, The Archive y Obsidian no están nada mal, pero es software de propietario, hay que pagar por algunas características y la información nunca será realmente tuya, pues quedarás atado a la herramienta y al modelo de negocios que adopten en un futuro. Si eso te tiene sin cuidado, go ahead (de nuevo, sin pensarlo mucho).

Aunque se adptan mejor a un sistema de tomar notas más afín a lo que buscaba Evernote ("dump everything here"), Bear, Zettlr, Joplin, Notion y demás apps de notas sirven. Hay ciertas cosas que tendrás que hacer manualmente, pero realmente no importa, pues el sistema se beneficia de un uso consciente y casi que manual de funciones como la implementación de enlaces o la generación de ids únicos. Con que te sientas a gusto con la interfaz es suficiente. 

Si lo que te interesa es complicarte las cosas, lo tuyo son las extensiones o paquetes para editores de texto. Escoge el de tu editor de preferencia y ya está. Y puesto que te quieres complicar, google ahead, my friend: hay complementos para Visual Studio, Vim, Emacs o Sublime (probablemente hay más, pero dejemos hasta ahí).

¿Cuál escogimos nosotros? [Sublime-ZK](https://github.com/renerocksai/sublime_zk), pues nos gusta complicarnos la vida, todo se maneja en archivos de texto plano así que a futuro siempre puede ser usada, sincronizar la base de datos entre dispositivos es tan sencillo como compartir la carpeta del proyecto y no quedamos atados a ninguna plataforma. Org-roam, la implementación en Emacs, nos generaba mucha curosidad, pero no tenemos el ánimo autodestructivo para emprender esa ruta (aún).