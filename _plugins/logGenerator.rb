module Jekyll
  class LogPageGeneratorLite < Generator
    safe true
    priority :highest

    def generate(site)
      colname = site.config["logColName"] # name of collection generated
      indexColname = site.config["indexLogColName"] # name of collection generated
      logDataFilename = site.config["logDataFilename"] # filename of datafile
      logData = site.data[logDataFilename]
      site.collections[colname] = Collection.new(site, colname)
      site.collections[indexColname] = Collection.new(site, indexColname)

      logItemNamedBy  = site.config["logItemsNamedBy"]
      logItemDir = site.config["logItemsDir"]

      # logData entero se accede asi:
      # puts "#{logData}"
      # un item de logsdata se accede mediante su index
      # puts "#{logData[0]}"
      # Y un item de ese hash se accede mediante...
      # puts "#{logData[0]["link"]}"
      
      count = 0 # number of pages generated
      index = 0 # index number of dataItem
      
      logPerDate = Array.new 
      parameterToGroupItems = nil

      logData.each do |logItem|
        index = index + 1
        # puts "!!!"
        # puts count
        if parameterToGroupItems == nil
          # puts "Primer item de data. No hay parametro. Nuevo parametro es #{logItem[site.config["logItemsGroupedBy"]]}"
          parameterToGroupItems = "#{logItem[site.config["logItemsGroupedBy"]]}"
        elsif "#{logItem[site.config["logItemsGroupedBy"]]}" != parameterToGroupItems
          # puts "El parametro de este logItem es diferente a las anteriores. Creamos pàgina de log hasta el momento y reiniciamos parametro: #{logItem[site.config["logItemsGroupedBy"]]}"
          count = count+1
          myPage = LogPageLite.new(site, site.source, logPerDate, count, colname, index-1, logItemNamedBy, logItemDir)
          site.collections[colname].docs << myPage
          logPerDate.clear
          # puts "reiniciamos logPerdate. nuevo length: "  + logPerDate.length.to_s 
          parameterToGroupItems = "#{logItem[site.config["logItemsGroupedBy"]]}"
        end
        logPerDate << logItem
        # puts "Agregamos nuevo item a logPerdate. nuevo length: "  + logPerDate.length.to_s       
        if logItem.equal?(logData.last)
          # puts "Ultimo item de la lista"
          count = count+1
          myPage = LogPageLite.new(site, site.source, logPerDate, count, colname, index, logItemNamedBy, logItemDir)
          site.collections[colname].docs << myPage
        end   
      end
      
      myLPage = LogIndexPageLite.new(site, site.source, logData, count, indexColname)
      site.collections[indexColname].docs << myLPage
  end

  class LogPageLite < Page
    def initialize(site1, base1, data1, count1, colname1, index1, logItemNamedBy1, logItemDir1)
      @site = site1
      @base = base1
      indexCounter = index1 - data1.length + 1 # index number of 1st item of set of data
      titleDir = "#{data1[0][logItemNamedBy1]}" # change value of this variable to:
        # count1 if you want items and urls named by counter or
        # indexCounter if you want them named by counter or
        # "#{data1[0][logItemNamedBy1]}" if you want items named by the config.yml variable

      @dir  = File.join(logItemDir1, titleDir) #output directory of page generated
      # if you want output urls according to count (logpage number), comment the previous line and uncomment the following one
      # @dir  = File.join(colname1, count1.to_s) #output directory of page generated

      @name = 'index.html'
      @count = count1

      self.process(@name)
      self.read_yaml(base1, site.config["logItemLayout"])
      self.data['rawLogData'] = data1.clone
      if site.config["nameOfDateFieldInData"] != nil
        self.data['date'] = "#{data1[0][site.config["nameOfDateFieldInData"]]}"
      end
      self.data['title'] = titleDir

      index = Array.new

      #creamos un array de keys de data1
      dataKeysArray = Array.new
      data1[0].each do |dataField|
        dataKeysArray << dataField[0]
      end

      #creamos un hash de arrays en donde alojaremos los valores que terminarán como variables de las páginas (tipo "page.visto")
      arrayHash = {}

      # Por cada key del array creado, creamos un array dentro del hash con su correspondiente key
      for key in dataKeysArray
        arrayHash[key] = Array.new
        #puts "creado array de:#{key} para logPage # #{count1}"
      end

      # por cada dataField (cada logItem) revisamos sus respectivos elementos (k, v) asignando al array de arrayHash con la key correspondiente el valor que le corresponde
      data1.each do |dataField|
        dataField.each do |k, v|
          arrayHash[k] << v
        end
        
        # y se itera el valor de index, por si necesitamos un identificador numérico para cada logItem
        index << indexCounter
        indexCounter = indexCounter + 1
      end
      
      # y finalmente después de haber agregado los elementos a arrayHash, le asignamos a cada variable de nombre k (visto, link, tipo, etc) de la página creada el array creado anteriormente
      arrayHash.each do |k, v|
        self.data[k] = v
      end

      # e index, pues este no es parte del csv de data
      self.data['index'] = index
      self.data['type'] = site.config["logType"]
      self.data['number'] = count1
      self.data['collection'] = colname1

    end
  end

  class LogIndexPageLite < Page
    def initialize(site1, base1, data1, count1, colname1)
      @site = site1
      @base = base1
      @dir  = site.config["logOutputFolder"]
      @name = 'index.html'
      @count = count1

      self.process(@name)
      
      self.read_yaml(base1, site.config["logFullLayout"])
      self.data['rawLogData']= data1.clone
      self.data['title']= site.config["logFullTitle"]
      self.data['collection']= colname1
      self.data['type']= site.config["logFullType"]
    end
  end

end
end